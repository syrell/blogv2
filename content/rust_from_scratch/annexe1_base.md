+++
title = "Les bases de nombres"
date = 2022-11-06
draft = false
path = "/rust/annex_bases"

[taxonomies]
categories = ["Rust par le métal (Annexe)"]
tags = ["rust", "rust_par_le_metal"]

[extra]
lang = "fr"
toc = true
show_comment = true

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Rust par le Métal(Annexe): Les bases de nombres" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/rust_bases.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Rust par le Métal(Annexe): Les bases de nombres" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/rust_bases.png" },
    { property = "og:url", content="https://lafor.ge/rust/annex_bases/" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]
+++

## Base 10

Lorsque vous demandez à un petit enfant de compter, il va généralement le faire sur ses doigts: 1, 2, 3, ..., 9, 10.

C'est alors qu'il va y avoir un retour à 1 avec un "je sais pas compter après 10 🥺".

On peut donc représenter cela sous la forme d'une roue graduée.

On introduit le 0 et on enlève le 10.

On lit la valeur sur la graduation rouge.

{{ image(path="rust/bases/roue1.png", width="50%" alt="Une roue graduée de 0 à 9 avec un curseur à 0") }}

Chaque fois que l'on tourne la roue, on avance d'une graduation.

{{ image(path="rust/bases/roue2.png", width="50%" alt="Une roue graduée de 0 à 9 avec un curseur à 1") }}

Et l'on peut aller comme cela, jusqu'à la graduation 9.

{{ image(path="rust/bases/roue3.png", width="50%" alt="Une roue graduée de 0 à 9 avec un curseur à 9") }}

Si on tourne la roue une fois de plus, on revient à 0.

{{ image(path="rust/bases/roue1.png", width="50%" alt="Une roue graduée de 0 à 9 avec un curseur à 0") }}

On ne peut donc pas compter jusqu'à 10 sur cette roue.

Sauf si on est ingénieux. 😎

On prend deux roue identique, et on relie la roue de gauche avec celle de droite.

A l'état initial, les deux roues sont à 0. On lit 0.

{{ image(path="rust/bases/roue4.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, les deux roues affichent 0") }}

Si on effectue on tourne de 1 graduation la roue de droite.

La roue de gauche entraînée par la courroie, tourne de 1/10 de graduation.

On lit alors 1 sur la roue de droite et quelque chose entre 0 et 1 sur la roue de gauche.

Mais que l'on considère comme valant 0.

En lisant les deux valeurs de roue on se retrouve avec "01" que l'on interprète comme un 1.

{{ image(path="rust/bases/roue5.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche, 0 la seconde 1") }}

Revenons à la situation où la roue de droite est à 9.

La roue de gauche est presque sur le 1, mais pas totalement.

Elle est donc toujours à 0.

On lit "09" donc 9.

{{ image(path="rust/bases/roue6.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche 0, la seconde 9") }}

On avance d'une dernière graduation la roue de droite, qui évidemment revient à 0. Mais la courroie entraîne une dernière fois la roue de gauche.

Qui finit par afficher 1. 

On lit alors "10" ou 10.

{{ image(path="rust/bases/roue7.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche 1, la seconde 0") }}

Ça y est, nous pouvons aller plus loin que 9. 🤩

On peut alors continuer à incrémenter jusqu'à 11.

{{ image(path="rust/bases/roue8.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, les deux roues affichent 1") }}

Ou même 19.

{{ image(path="rust/bases/roue9.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche 1, la seconde 9") }}

À 19, une incrémentation supplémentaire nous fait passer à 20.

{{ image(path="rust/bases/roue10.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche 2, la seconde 0") }}

Continue, jusqu'à 99.

{{ image(path="rust/bases/roue11.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche 9, la seconde 9") }}

Si on ne fait rien, on se retrouve devant le même problème que l'enfant qui recommençait à 1 après 10.

Nos roues, reviennent à leur état initial.

{{ image(path="rust/bases/roue4.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, en allant de gauche à droite, la première affiche 1, la seconde 0") }}

Du coup la solution est donc de rajouter une roue.

À 99, cela donne ça:

{{ image(path="rust/bases/roue12.png", width="75%" alt="Deux roues graduées de 0 à 9 reliée par une courroie, les deux roues affichent 9") }}

À 100, ceci:

{{ image(path="rust/bases/roue13.png", width="75%" alt="Trois roues graduées de 0 à 9 reliée par des courroies, en allant de gauche à droite, la première affiche 1, la seconde 0, la troisième 0") }}

On peut rajouter autant de roue que l'on souhaite pour représenter des nombres de plus en plus grands.

{{ image(path="rust/bases/roue14.png", width="75%" alt="Une série de 5 roues reliées par une couroie chacune qui affichent respectivement 7, 5, 2, 1, 9 en allant de la gauche vers la droite") }}

{{ image(path="rust/bases/roue16.png", width="75%" alt="Une multitude de roues reliées par une courroie") }}

Un nombre très petit peut être représenté sur un très grand nombre de roues, il suffit de mettre les roues à la gauche du nombre à 0.

{{ image(path="rust/bases/roue15.png", width="75%" alt="Une série de 5 roues reliées par une couroie chacune qui affichent respectivement 0, 0, 0, 0, 1 en allant de la gauche vers la droite. Plus une phrase : on lit 1 au dessus") }}

On nomme `base`, le nombre de cran de la roue.

Ici, 10 crans, c'est donc de la base 10.

Cette base a un petit nom, on l'appelle la `base décimale`.

{{ image(path="rust/bases/base10.png", width="75%" alt="Une roue graduée de 0 à 9 affichant 1. Et une mention : 10 crans sur une roue, base 10") }}

### Mathématiques

En primaire, on nous a parlé des dizaines et des unités, et bien c'est le nom que l'on va donner à nos roue.

On va dire que les dizaines sont en rouge et les unités en jaune.

{{ image(path="rust/bases/base_10_2.png", width="75%" alt="Deux roue avec respectivement les mentions dizaines et unités. Affichant de gauche à droite 1 et 2") }}

Le nombre en violet "compte" combien de fois la roue jaune à réalisé de tours complets.

C'est en quelque sorte une mémoire.

Nous allons renommer les roues pour plus de simplicité en fonction de leur place en partant de la droite pour aller vers la gauche.

{{ image(path="rust/bases/base_10_8.png", width="75%" alt="Deux roues avec respectivement les mentions roue 1 et roue 0. Affichant de gauche à droite 1 et 2") }}

Il y a 10 graduation sur la roue 0, 1 graduation de la roue 1 équivaut donc à 10 graduations de la roue 0. 

{{ image(path="rust/bases/base_10_3.png", width="75%" alt="Une équivalence entre une graduation de la roue 1 et 10 de la roue 0") }}

On introduit une nouvelle notation:

{{ image(path="rust/bases/base_10_6.png", width="75%" alt="notation en base 10 de 12") }}

Puis on utilise les puissance de 10 pour généraliser l'écriture.

{{ image(path="rust/bases/base_10_7.png", width="75%" alt="Décomposition en puissance de 10 du nombre 12. 1 fois 10 puissance 1 + 2 fois 10 puissance 0") }}

Si l'on a plus de roues, le principe reste identique

{{ image(path="rust/bases/base_10_5.png", width="75%" alt="Trois roues. Affichant de gauche à droite 1, 6 et 3") }}

Que l'on décompose en 

{{ image(path="rust/bases/base_10_9.png", width="75%" alt="Décomposition en puissance de 10 du nombre 163. 1 fois 10 puissance 2 + 6 fois 10 puissance 1 + 3 fois 10 puissance 0") }}

## base 2

Il est tout a fait possible de faire varier le nombre de crans de la roue.

Si l'on n'a que 2 crans, on passe en base 2, ou plus généralement appelée `binaire`.

{{ image(path="rust/bases/base2.png", width="50%" alt="Une roue graduée de 0 à 1 affichant 0. Et une mention : 2 crans sur une roue, base 2") }}

Cela fonctionne de la même manière. 

{{ image(path="rust/bases/roue_2_1.png", width="50%" alt="Une roue graduée de 0 à 1 affichant 0") }}

Une roue graduée, chaque graduation faisant un demi tour.

{{ image(path="rust/bases/roue_2_2.png", width="50%" alt="Une roue graduée de 0 à 1 affichant 1") }}

Si l'on fait un tour complet, on se retrouve à l'état initial.

{{ image(path="rust/bases/roue_2_1.png", width="50%" alt="Une roue graduée de 0 à 1 affichant 0") }}

Pour palier le soucis on rajoute une deuxième roue à deux graduations.


{{ image(path="rust/bases/roue_2_5.png", width="50%" alt="Deux roues graduées de 0 à 1 affichant 0") }}

Et on recommence.

{{ image(path="rust/bases/roue_2_7.png", width="50%" alt="Deux roues graduées de 0 à 1 affichant respectivement de gauche à droite 0 et 1") }}

En parrallèle, on fait tourner une roue décimale.

{{ image(path="rust/bases/roue2.png", width="25%" alt="Une roue graduée de 0 à 9 affichant 1") }}

On peut alors décaler d'une graduation supplémentaire sans perdre le compte.

{{ image(path="rust/bases/roue_2_3.png", width="50%" alt="Deux roues graduées de 0 à 1 affichant respectivement de gauche à droite 1 et 0. On lit 10") }}

Dans le même temps, notre roue décimale affiche 

{{ image(path="rust/bases/roue_10_1.png", width="25%" alt="Une roue graduée de 0 à 9 affichant 2") }}

On continue, la roue de gauche se déplace encore d'un quart de tour.

{{ image(path="rust/bases/roue_2_4.png", width="50%" alt="Deux roues graduées de 0 à 1 affichant respectivement de gauche à droite 1 et 1. On lit 11") }}

Nous arrivons à 3 sur la roue décimale.

{{ image(path="rust/bases/roue_10_2.png", width="25%" alt="Une roue graduée de 0 à 9 affichant 3") }}

Encore une graduation à gauche et "00". 😥

{{ image(path="rust/bases/roue_2_5.png", width="50%" alt="Deux roues graduées de 0 à 1 affichant 0. On lit 0") }}

Alors que l'on a 4 sur la roue décimale.

{{ image(path="rust/bases/roue_10_3.png", width="25%" alt="Une roue graduée de 0 à 9 affichant 4") }}

On rajoute une roue. Pour être dans la capacité de représenter "4".

{{ image(path="rust/bases/roue_2_6.png", width="50%" alt="Trois roues graduées de 0 à 1 affichant respectivement de gauche à droite 1, 0 et 0. On lit 100") }}

### Mathématiques

Faisons tourner la roue une dernière fois. On obtient 5 en base 10.

On renomme nos roues des `bits`.

Et l'on numérote comme l'on a pu le faire en base décimale.

{{ image(path="rust/bases/base_2_1.png", width="100%" alt="Trois roues nommées respectivement bit 2, bit 1 et bit 0 qui affichent 1, 0, 1.") }}

On peut alors réutiliser notre notation.

{{ image(path="rust/bases/base_2_4.png", width="100%" alt="Notation en base 2 de 101") }}

Cette fois-ci, il faut faire tourner 2 fois la roue 0 pour décaler d'une graduation la roue 1.

{{ image(path="rust/bases/base_2_2.png", width="100%" alt="Une équivalence entre une graduation de bit 1 et 2 de bit 0") }}

Il est alors possible de réaliser la décomposision cette fois-ci en puissance de 2.

{{ image(path="rust/bases/base_2_5.png", width="100%" alt="Décomposition en somme de produit de puissance de 2 de 101. 1 fois 2 puissance 2 + 0 * 2 puissance 1 + 1 fois 2 puissance 0. Ce qui donne 5 en écriture décimale") }}

Ce qui se généralise très bien.

{{ image(path="rust/bases/base_2_6.png", width="100%" alt="Trois roues nommées respectivement bit 3, bit 2, bit 1 et bit 0 qui affichent 1, 1, 0, 0.") }}

En

{{ image(path="rust/bases/base_2_7.png", width="100%", alt="Décomposition en somme de produit de puissance de 2 de 1100. 1 fois 2 puissance 3 + 1 fois 2 puissance 2 + 0 * 2 puissance 1 + 0 fois 2 puissance 0. ") }}

Ce nombre est généralement noté en informatique `0b1100`.

## Base 16

Maintenant que l'on est rodé, on peut jouer avec la définition des bases.

On peut par exemple rajouter des graduation à notre roue et en faire une à 16 graduation.

Comme on n'a plus de chiffres, on passe au lettres.

On nomme cette base 16, la base `héxadécimale`.

{{ image(path="rust/bases/base_16.png", width="50%" alt="Une roue graduée de 0 à 9 puis de A à F, affichant E") }}

On se créé un nombre héxadécimale `A3`.

{{ image(path="rust/bases/base_16_3.png", width="50%" alt="Deux roues graduées de 0 à F, affichant respectivement A et 3") }}

Que l'on décompose en:

{{ image(path="rust/bases/base_16_2.png", width="100%" alt="Décomposition en somme de puissance de 16 de A3. A fois 16 puissance 1 + 3 fois 16 puissance 0. Donne: 10 fois 16 puissance 1 + 3 fois 16 puissance 0. Ce qui donne 163 en écriture décimale.") }}

Ce nombre est généralement noté en informatique `0xA3` ou `0xa3`.

## Nombre à base composite

Vous l'utilisez tous les jours.

Ce sont les secondes, minutes, heures, jours et semaines.

- 60 secondes => 1 minute
- 60 minutes => 1 heure
- 24 heures => 1 journée
- 7 journées => 1 semaines 

{% tip(header="le saviez-tu") %}
La base 12 provient du nombre de phalanges dans une main. Si on en utilise 2, on arrive à une base 24.

La base 60, est constitué de la base 12 d'une main et des 5 doigts de l'autre main 5*12 = 60.

6 phalanges 3 doigts => 6 + 3*12 = 36 + 3 = 39

{% end %}

{{ image(path="rust/bases/jour1.png", width="100%" alt="Quatre roues ayant respectivement de gauche à droite les mentions base 7, base 24, base 60 et base 60. Et ayant respectivement les valeurs 4, 15, 28 et 35. Une phrase 4 jours 15 heures 28 minutes  et 35 secondes en dessous.") }}

On peut alors procéder à la décomposition.

{{ image(path="rust/bases/jour2.png", width="100%" alt="somme de produits: 4 fois 24 fois 6 fois 60 + 15 fois 60 fois 60 + 28 fois 60 + 35 égal 401315 secondes.") }}

En procédant à la même décomposition lorsque toutes les roues sont en base 2.

L'on peut comprendre pourquoi la notation en puissance de 2 est possible.

Il s'agit d'un cas particulier lorsque les bases sont identiques.

{{ image(path="rust/bases/jour3.png", width="100%" alt="Quatre roue avec la mention base 2. Avec les valeurs de 1,1,0 et 0. Puis une équantion. 1 fois 2 fois 2 fois 2 + 1 fois 2 fosi 2. Donne 1 fois 2 puissance 3 + 1 fois 2 puissance 2 +  0 fois 2 puissance 1 + 0 fois 2 puissance 0.") }}

## Conclusion

Un même nombre peut avoir une infinité de représentation différentes.

Certaines représentations possèdent des noms:

- représentation décimale : `28`
- représentation  héxadécimale : `0x1c`
- représentation binaire : `0b11100`

Ces représentation ont pour particularité d'avoir une base constante.
