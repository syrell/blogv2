+++
title = "Types primitifs"
date = 2022-10-31
draft = true
path = "/rust/types"

[taxonomies]
categories = ["Rust par le métal"]
tags = ["rust"]

[extra]
lang = "fr"
toc = true
show_comment = true
+++

Rust est un langage statiquement typé, ce qui signifie qu'à la compilation chaque [variable](/rust/variable) se doit de posséder un type.

## Scalaires

Il existe plusieurs types primitifs:

- nombres entiers
- nombres flottants
- booléens
- char

On appelle cette ensemble de types les scalaires car l'on peut les représenter avec un unique nombre.

Mais pour cela nous devons comprendre ce qu'est un nombre pour un ordinateur. 😁

### Entiers naturels

Il s'agit des nombres 0, 1, 2, 3, 4, ...., 999, 10000000, etc.

C'est la forme la plus simple que peut prendre un nombre.

Il faut d'abord comprendre ce que l'on stocke.

Un ordinateur ne sait compter que jusqu'à `1`.

Ou plus exactement il ne connait que deux signes `0` ou `1`, le fameux langage binaire.

- Pour représenter le `0` de notre système décimal on utilise le `0` du binaire.
- Pour représenter le `1` de notre système décimal on utilise le `1` du binaire.

Mais là ou ça se corse c'est pour représenter `2`. On n'a pas de `2` en binaire.

Comment faire ?

Et bien de la même façon que lorsqu'on écrit `10` comme après `9`, il n'existe plus de chiffre, on revient à `0` et on écrit `1` à sa gauche.

En binaire on fait pareil.

- Pour représenter le `2` de notre système décimal on utilise le `10` du binaire.
- Pour représenter le `3` de notre système décimal on utilise le `11` du binaire.
- Pour représenter le `4` de notre système décimal on utilise le `100` du binaire.

Et ainsi de suite. 🙂

Ok, donc on stocke les nombre entiers avec des `0` et des `1`. 

Parfait, elle est où la difficulté ?

Pour comprendre, il va falloir encore plus descendre vers le métal. 😎

Un ordinateur est composé de plusieurs éléments, mais les plus vitaux sont le CPU et la RAM (l'alimentation aussi ⚡).

Le CPU et la RAM sont relié par un bus de données (un ensemble de fils) de 64 fils dans les ordinateurs modernes. 

32 fils pour les ordinateurs plus vieux et 16 voir 8 pour la préhistoire. 

{% tip() %}
Ce nombre de fils est appelé une architecture:
- 64 fils => architecture 64 bits
- 32 fils => architecture 32 bits
{% end %}