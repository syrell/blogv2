+++
title = "Fabriquer son Blog en Rust"
date = 2022-11-29
draft = true
path = "/blog_rust"

[taxonomies]
categories = ["Rust"]
tags = ["rust"]

[extra]
lang = "fr"
toc = true
show_comment = true

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="Fabriquer son blog en Rust" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/blog_rust.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="Fabriquer son blog en Rust" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/blog_rust.png" },
    { property = "og:url", content="https://lafor.ge/refinement_type/" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]
+++

Bonjour à toutes et tous 🙂

Lors de la conférence [Very Tech Trip](https://verytechtrip.com/) qui s'est déroulé le 2 février 2023, j'ai eu le plaisir de présenter la création d'un blog minimal en 
Rust.

Mais en seulement 30 min. 😭

Autant le dire tout de suite ce n'était pas suffisant pour pouvoir rentrer dans le détail.

Cet article est donc un développement du propos de la conférence. Qui aurait tenu dans 1h30! 😁

On a du boulot, donc commençons ! 😃.

## et ses outils

Avant d'aller à la bataille, il nous faut des armes.

Et ça tombe bien, les créateurs de Rust ont pensé leur langage pour faciliter son utilisation.

### Rustup

Installer Rust et tous ces outils est rendu pratique grâce au site [rustup.rs](https://rustup.rs).

Celui-ci, propose une variété de manière d'installer la chaîne de développement de rust.

Quelle que soit la méthode que vous allez choisir, vous allez installer plusieurs choses dans votre espace personnel.

- rustc : le compilateur de 

### Cargo

## Hello World

## Hello World Actix Web

## Ma première route

### Extraction de paramètre

## Les services

## Templating

### State d'application

### Tera

### Contexte

## Diesel et base de données

### Migrations

### Schéma

### ORM

## Créer des comptes utilisateurs

### Templates de page

### Hachage du mot de passe

### Vérification du formulaire

### Insertion

### Redirection

## Session utilisateur

### Middleware

### Cookies de session

## Gestions des posts

### Macro de templates

### Jointure de table

## Configuration

### Figment

## Conclusion