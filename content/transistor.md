+++
title = "Transistor"
date = 2023-01-12
draft = true
path = "/transistor"

[taxonomies]
categories = ["Hardware"]
tags = ["hardware"]

[extra]
lang = "fr"
toc = true
show_comment = true
math = true

metas = [
    { name = "twitter:card", content="summary_large_image" },
    { name = "twitter:title", content="La mémoire physique" },
    { name = "twitter:image", content="https://lafor.ge/assets/thumbails/physical_memory2.png" },
    { property = "og:type", content="website" },
    { property = "og:title", content="La mémoire physique" },
    { property = "og:image", content="https://lafor.ge/assets/thumbails/physical_memory2.png" },
    { property = "og:url", content="https://lafor.ge/physical_memory" },
    { property = "og:image:width", content="1200" },
    { property = "og:image:heigth", content="675" },
]
+++

Le transistor est peut-être la chose que l'on m'a le plus mal expliqué lors de mes études.

Pourtant, celui-ci est à la base du monde moderne, et me fait littéralement vivre en tant que développeur.

Avant de commencer, je fais un petit disclaimer :

Ceci n'est pas un cours ! Si vous êtes étudiant(e) ne mettez pas ça dans votre copie, vous allez vous faire fumer au bois de hêtre par votre prof. 😁

Prenez ça comme un épisode de "C'est pas Sorcier", une introduction vers quelque chose de plus vaste.

Bien commençons 😀

## Electricité

### Pile

### Intensité

### Tension

### Résistance

### Condensateur

## Semi-conducteur

### Silicium

### Dopage

### Jonction PN

## Transistor Bipolaire

### Effet transistor

## Transistor MOFSET

### Effet de champ

## Conclusion