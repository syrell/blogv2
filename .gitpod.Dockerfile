FROM gitpod/workspace-full

RUN wget https://github.com/barnumbirr/zola-debian/releases/download/v0.16.1-1/zola_0.16.1-1_amd64_bullseye.deb && \
    sudo dpkg -i zola_0.16.1-1_amd64_bullseye.deb && \
    rm zola_0.16.1-1_amd64_bullseye.deb